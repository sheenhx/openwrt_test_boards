# OPENWRT SAMPLE BOARDS #

This repo would normally document whatever steps are necessary to get the AR9344,QCA9558 boards up and running.

### QCA9558+QCA9880 Dual-band AC1750 ###
![IMG_7447_meitu_1.jpg](https://bitbucket.org/repo/99nda6/images/1353407010-IMG_7447_meitu_1.jpg "Title of your image" =516x140)

* Share Archer C7 V2 profile with TP-link
* 16MB flash 128MB RAM
* 2.4G 3X3 5G 3X3 11ac

*USB sound card*

* plan to use C-media CM119B crystal-less chip (1.6 USD)
* just need `opkg install kmod-usb-audio`

### AR9344+AR9580+8327 ###
![IMG_7449_meitu_3.jpg](https://bitbucket.org/repo/99nda6/images/307754484-IMG_7449_meitu_3.jpg)

* Share TP-WDR4310 profile with TP-link
* 16MB flash 128MB RAM (need to modify the openwrt profile to suit 16MB, since original 4310 is 8MB)
* 2.4G 2X2 5G 3X3 11n

This board can flash our openwrt firmware for WDR4310

### AR9344+AR9382+4G mPcie slot ###

![IMG_7448_meitu_2.jpg](https://bitbucket.org/repo/99nda6/images/778836892-IMG_7448_meitu_2.jpg)

* Share TP-WDR3500 v1 profile with TP-link
* 8MB flash 128MB RAM
* 2.4G 2X2 5G 2x2 11n

***How to use 4G mPCIe card***

* define             pin           active   

* LAN   LED          GPIO19（C15）   Low
* WAN  LED           GPIO18（B16）   Low
* 2.4G WLAN LED      GPIO13（N26）   Low
* 5.8G WLAN 	   9582_GPIO0(41)  Low
* Reset Button       GPIO17 (M25)    Low
* 4G MODULE power    GPIO0     High for power up, low for power down


* Note:
* WAN port
* AD27,AE27,AF27,AG27
* LAN1 port
* AG20,BAF20,A4G19,AF19


Necessary packages for ZTE Zm8620 4G MODULE:
`kmod-usb-net`
`wwan`